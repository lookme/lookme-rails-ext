# lookme-rails-ext

## 紹介

Rails機能拡張

## 機能

### リクエストログ

Rails4では、Rails::Rack::Loggerなぜかログとrequest subscriberは一緒でして、
静的ファイルのリクエストをミュートしたり、APIサービスである文字列から始めるリクエストだけ見たい時は、簡単にできないです。
一方、Rails::Rack::Loggerをmiddleware pipelineから消すとrequest subscriberが動作しなくなるのも困ります。
もしRails::Rack::Loggerは2つの役割ではなくて、別々に分ければいいと思ったので、分けたmiddlewareを作りました。

使い方としてはapplication.rbに

    config.middleware.swap 'Rails::Rack::Logger', 'LookmeRailsExt::RequestSubscriber'
    config.middleware.insert_before 'LookmeRailsExt::RequestSubscriber', 'LookmeRailsExt::ExecutionInterceptor'

を追加して、Rails::Rack::Loggerの機能をRequestSubscriberとExecutionInterceptorに分けられました。

RequestSubscriberは名前通り、request subscriberの機能だけあります。

ExecutionInterceptorは各リクエストのライフサイクルに機能を追加できるmiddlewareです。

もちろん、ログ機能があります。それに、使いやすくためにフィルターも加えました。

静的ファイルのリクエストは出したくない時は

    LookmeRailsExt::ExecutionInterceptor.add_listeners(
        [
            LookmeRailsExt::RequestLogging.new(->(request) {
              !request.path.start_with?('/assets')
            })
        ]
    )

APIのリクエストだけ出したい時は

    LookmeRailsExt::ExecutionInterceptor.add_listeners(
        [
            LookmeRailsExt::RequestLogging.new(->(request) {
              request.path.start_with?('/api')
            })
        ]
    )

