module LookmeRailsExt
  class RequestLogging < ExecutionListener
    LOG_TAG = self.name

    def initialize(filter = nil)
      @logger = LookmeLogging::LoggerManager.find_logger(self.class)
      @filter = filter
    end

    def pre_call(env, request)
      return if skip_request?(request)
      if ::Rails.env.development?
        @logger.debug(LOG_TAG) {''}
        @logger.debug(LOG_TAG) {''}
      end
      @logger.info(LOG_TAG) {started_request_message(request)}
    end

    private

    def skip_request?(request)
      return false if @filter.nil?
      !@filter.call(request)
    end

    # Started GET "/session/new" for 127.0.0.1 at 2012-09-26 14:51:42 -0700
    def started_request_message(request)
      'Started %s "%s" for %s at %s' % [
          request.request_method,
          request.filtered_path,
          request.ip,
          Time.now
      ]
    end
  end
end