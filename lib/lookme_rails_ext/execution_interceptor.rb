module LookmeRailsExt
  class ExecutionInterceptor
    def initialize(app)
      @app = app
      @listener = CompositeExecutionListener.new(self.class.listeners)
    end

    def call(env)
      request = ActionDispatch::Request.new(env)
      @listener.pre_call(env, request)
      begin
        response = @app.call(env)
        @listener.post_call(env, request, response)
        response
      rescue => e
        @listener.on_exception(env, request, e)
        raise e
      end
    end

    class << self
      def add_listener(listener)
        listeners << listener
      end

      def add_listeners(ls)
        listeners.concat(ls)
      end

      def listeners
        @listeners ||= []
      end
    end
  end
end