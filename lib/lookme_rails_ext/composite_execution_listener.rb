module LookmeRailsExt
  class CompositeExecutionListener < ExecutionListener
    LOG_TAG = self.name

    def initialize(listeners = [])
      @logger = LookmeLogging::LoggerManager.find_logger(self.class)
      @listeners = listeners
    end

    def pre_call(env, request)
      apply {|l| l.pre_call(env, request)}
    end

    def on_exception(env, request, exception)
      apply {|l| l.on_exception(env, request)}
    end

    def post_call(env, request, response)
      apply {|l| l.post_call(env, request, response)}
    end

    private

    def apply(&block)
      return if @listeners.empty?
      begin
        @listeners.each {|l| block.call(l)}
      rescue => e
        @logger.warn(LOG_TAG) {e}
      end
    end
  end
end