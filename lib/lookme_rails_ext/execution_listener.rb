module LookmeRailsExt
  class ExecutionListener
    def pre_call(env, request)
    end

    def on_exception(env, request, exception)
    end

    def post_call(env, request, response)
    end
  end
end