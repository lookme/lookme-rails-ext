module LookmeRailsExt
  class RequestSubscriber < ActiveSupport::LogSubscriber
    def initialize(app, taggers = nil)
      @app = app
    end

    def call(env)
      request = ActionDispatch::Request.new(env)
      call_app(request, env)
    end

    protected

    def call_app(request, env)
      instrumenter = ActiveSupport::Notifications.instrumenter
      instrumenter.start 'request.action_dispatch', request: request
      resp = @app.call(env)
      resp[2] = ::Rack::BodyProxy.new(resp[2]) {finish(request)}
      resp
    rescue Exception
      finish(request)
      raise
    ensure
      ActiveSupport::LogSubscriber.flush_all!
    end

    private

    def finish(request)
      instrumenter = ActiveSupport::Notifications.instrumenter
      instrumenter.finish 'request.action_dispatch', request: request
    end
  end
end
