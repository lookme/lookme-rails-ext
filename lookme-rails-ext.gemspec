# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'lookme_rails_ext/version'

Gem::Specification.new do |s|
  s.name = 'lookme-rails-ext'
  s.version = LookmeRailsExt::VERSION
  s.authors = ['chen.zhao']
  s.summary = 'lookme rails ext'

  s.add_runtime_dependency 'lookme-logging', '~> 0.0.1'
  s.add_runtime_dependency 'actionpack', '>= 4.0'
  s.add_runtime_dependency 'activesupport', '>= 4.0'
  
  s.files = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  s.require_paths = ['lib']
end